class Studenci
  attr_accessor :names 
  def initialize(*args)
    @names = []
    args.to_a.each{|i| @names << i.to_s }
  end
  def method_missing(name, *args)

  	return @names.include?(name.to_s.delete"?")
  end
end

stud = Studenci.new("ania", "zosia")
print stud.zosia?
print stud.krysia?

describe Studenci do
  before :each do 
    @student = Studenci.new("zosia","asia","gosia")
  end
  describe "#new" do 
    it "return a Studenci object" do 
      expect(@student).to be_an_instance_of Studenci
    end	
  end
  describe "#names" do
    it "returns a true if name is in array @names" do
      expect(@student.names).to include("zosia")
    end	
  end
  describe "#method_missing" do 
    it "returns true if name is in array @names " do
      expect( @student.zosia?).to eq true 
    end
    it "returns false if name IS NOT in array @names" do
      expect( @student.krysia?).to eq false 
    end
  end    
end

