#!/usr/bin/ruby
class Human 
  attr_reader :name
  def initialize(name)
    @name = name
  end 
end

class Student < Human 
  def method_missing(method_name, *args)
    if method_name.to_s.match(@name+"?")
      puts "true"
    else
      puts "false"   	
    end
  end
end
b = Student.new("zosia")
b.zosia?
b.kasia?

