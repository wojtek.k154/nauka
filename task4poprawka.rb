#!/usr/bin/ruby
class Student
  include Comparable
  #include Enumerator
  attr_reader :srednia
  def initialize(* args)
    @srednia = 0.0
    @srednia = args.inject {|sum, u| sum + u}/args.size
  end
  def <=>(otherSrednia)
    self.srednia <=> otherSrednia.srednia
  end
end

class Group
  include Enumerable
  attr_reader :gr
  def initialize(*args)
    @gr = []
    args.to_a.each do |u| 
      @gr << u.srednia.round(2)
    end
    @gr.to_a.sort! {|a,b| a<=>b }
    @gr = @gr.reverse
  end
  def [](i)
    print @gr[i]
  end
  def each
   @gr.each{ |e| yield(e) }
  end
end

stud = Student.new(3.0,3.0,5.0,5.0,4.0,5.0)
stud2 = Student.new(3.5,4.5,4.0,4.0,3.0,4.5)
st = Student.new(3.5,5.0,5.0,5.0,4.5,5.0)
stud3 = Student.new(5.0,5.0,5.0,5.0)
print stud.srednia.round(2)
print "\n#{stud2.srednia.round(2)} \n"
print "#{stud < stud2}\n\n"
b = Group.new(stud, stud2, stud3, st)
print b[0]
print "\n\n"
#t=[]

b.each do|p|
 	puts p
end
print "\n\n"
b.map{|u| puts u}
print "\n\n"