#!/usr/bin/ruby
class Human 
  attr_accessor :name
  def initialize(name)
    @name = name
  end 
end

class Student < Human 
  def method_missing(method_name, *args)
    return method_name.to_s == @name+"?"
  end
end

b=Student.new("zosia")
print b.zosia?
print b.ania?
	
describe Student do
  before :each do 
    @student = Student.new("zosia")
  end
  describe "#new" do 
    it "return a Student object" do 
      expect(@student).to be_an_instance_of Student
    end
    it "Should return a raise exeption if number of parameters will be different than 1" do
      expect{Student.new("zosia", "Age")}.to raise_exception(ArgumentError)
    end
  end
  describe "#name" do
    it "returns a true if name is correct" do
      expect(@student.name).to eq "zosia"
    end	
  end
  describe "#method_missing" do	
    it "should return true if name parameter is equal value of name field" do
      expect( @student.zosia?).to eq true 
    end
    it "should return true if name parameter is NOT equal value of name field" do
      expect( @student.ania?).to eq false 
    end
  end    
end

