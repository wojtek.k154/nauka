#!/usr/bin/ruby
#---------------------------------------------------------
class Student
	attr_accessor :first_name ,:last_name, :srednia, :grupa
	def initialize(fname, lname, sr, gr )
		 @first_name = fname
		 @last_name = lname
		 @srednia = sr
		 @grupa = gr		
	end
	#def new(fname, lname, sr, gr )
	#	Student.new(fname, lname, sr, gr)
	#end

end
#------------------------------------------------------
class Grupa
	#require 'sort'
	def initialize()
		@grupa = []
	end

	def add(student)
		@grupa << student
	end

	def show(n)
		@grupa.sort! {|x| x.srednia }
		@grupa.reverse!
		@grupa.each do |item|
			print " #{item.first_name} , #{item.last_name}, #{item.srednia}, #{item.grupa} \n " if item.grupa == n.to_i
		end
	end
	def comp(fname1, lname1, fname2, lname2)
		student1 = nil
		student2 = nil 
		@grupa.each do |x|
			student1 = x if x.first_name == fname1 && x.last_name == lname1
			student2 = x if x.first_name == fname2 && x.last_name == lname2
		end
		if (student1.srednia <=> student2.srednia) == 1
			print "#{student1.first_name} #{student1.last_name} has greater average than student #{student2.first_name} #{student2.last_name}"
		elsif (student1.srednia <=> student2.srednia) == (-1)
			print "#{student2.first_name} #{student2.last_name} has greater average than student #{student1.first_name} #{student1.last_name}"
		elsif (student1.srednia <=> student2.srednia)
			print "#{student2.first_name} #{student2.last_name} has the same average as student #{student1.first_name} #{student1.last_name}"
		end
	end
	def average(n)
		sr = 0.0
		oc = []
		@grupa.select{|u| oc << u.srednia if u.grupa == n.to_i}
		sr = oc.inject{|sum, el| sum+el } / oc.size
		print " srednia grupy #{n} wynosi: #{sr} \n " 
	end
end

#----------------------------------
gr = Grupa.new
student = Student.new("Adam", "Maslanka", 4.5,1)
gr.add(student)
student = Student.new("Roman", "Losichko", 4.0,1)
gr.add(student)
student = Student.new("Tomasz", "Marchewka", 4.5,1)
gr.add(student)
student = Student.new("Yura", "Yakym", 3.0,1)
gr.add(student)
student = Student.new("Piotr", "Maslanka", 5.0,1)
gr.add(student)
student = Student.new("Jakub", "Krupski", 4.5,2)
gr.add(student)
student = Student.new("Agnieszka", "Krawczyk", 5.0,2)
gr.add(student)
student = Student.new("Wojtek", "Kustrzyk", 4.5,2)
gr.add(student)
student = Student.new("Grzegorz", "Kołodziej", 4.5,2)
gr.add(student)
 gr.show(1)
print "\n"
gr.comp("Roman", "Losichko","Wojtek", "Kustrzyk")
print "\n"
gr.average(2)