#!/usr/bin/ruby
class Student
	include Comparable
	attr_reader :srednia, :init
	def initialize(* args)
		@srednia = 0.0
		@srednia = args.inject {|sum, u| sum + u}/args.size
	end
	def <=>(otherSrednia)
		self.srednia <=> otherSrednia.srednia
	end
end

stud = Student.new(3.0,3.0,5.0,5.0,4.0,5.0)
stud2 = Student.new(3.5,4.5,4.0,4.0,3.0,4.5)
print stud.srednia.round(2)
print "\n#{stud2.srednia.round(2)} \n"
print "#{stud < stud2}\n"